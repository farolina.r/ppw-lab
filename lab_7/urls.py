from django.conf.urls import url
from .views import index, friend_list, friend_list_json, add_friend, delete_friend, validate_npm, validate_nama

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-friend/$', add_friend, name='add-friend'),
    url(r'^validate-npm/$', validate_npm, name='validate-npm'),
    url(r'^validate-nama/$', validate_nama, name='validate-nama'),
    url(r'^delete-friend/(?P<friend_id>[0-9]+)/$', delete_friend, name='delete-friend'),
    url(r'^get-friend-list/$', friend_list, name='get-friend-list'),
    url(r'^get-friend-list-json/$', friend_list_json, name='get-friend-list-json'),
    url(r'delete-friend/(?P<npm>[0-9]+)/$', delete_friend, name='delete-friend'),
	url(r'delete-friend//$', delete_friend, name='delete-friend'),
    url(r'delete-friend/(?P<npm>[0-9]+)/$', delete_friend, name='delete-friend'),

]
